export class Snake {
  constructor({
    direction,
    positions
  }) {
    this.direction = direction;
    this.newDirection = direction;
    this.positions = positions;
    this.eatNow = false;
  }
  setDirection(newDirection) {
    if (newDirection in Snake.directions && newDirection !== Snake.directionsBlocks[this.direction]) {
      this.newDirection = newDirection;
    }
  }
  eat() {
    this.eatNow = true;
  }
  move(screenSize, wallsPositions) {
    const head = {};
    const move = Snake.directions[this.newDirection];
    head.x = this.positions[0].x + move.x;
    head.y = this.positions[0].y + move.y;
    if (head.x < 0) {
      head.x = screenSize[0] - 1;
    }
    if (head.y < 0) {
      head.y = screenSize[1] - 1;
    }
    if (head.x >= screenSize[0]) {
      head.x = 0;
    }
    if (head.y >= screenSize[1]) {
      head.y = 0;
    }
    if (this.positions.find(({x, y}) => x === head.x && y === head.y)) {
      return false;
    }
    if (wallsPositions[`${head.x}-${head.y}`]) {
      return false;
    }
    const positions = [
      head,
      ...this.positions,
    ];
    this.direction = this.newDirection;
    this.positions = positions.splice(0, positions.length - (this.eatNow ? 0 : 1));
    this.eatNow = false;
    return true;
  }
}

Snake.directions = {
  up: {
    x: 0,
    y: -1,
  },
  down: {
    x: 0,
    y: 1,
  },
  left: {
    x: -1,
    y: 0,
  },
  right: {
    x: 1,
    y: 0,
  },
};

Snake.directionsBlocks = {
  up: 'down',
  down: 'up',
  left: 'right',
  right: 'left',
};

Snake.color = 'green';
Snake.headColor = 'blue';

