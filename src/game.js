import randomInteger from 'rand-int';

import {Snake} from './snake';

import level0 from './level0.json';
import level1 from './level1.json';
import level2 from './level2.json';
import level3 from './level3.json';

const levels = [level0, level1, level2, level3];

export class Game {
  constructor(canvas, level) {
    this.canvas = canvas;
    this.ctx = canvas.getContext('2d');
    this.level = level;

    Game.musroomcolor = 'red';
    Game.wallcolor = 'gray';
  }

  init() {
    const currentLevel = levels[this.level];
    if (currentLevel) {
      this.screenSize = currentLevel.screenSize;
      this.zoom = currentLevel.zoom;
      this.target = currentLevel.target;
      let { walls, wallsSchema } = currentLevel;
      if (wallsSchema) {
        walls = [];
        wallsSchema.forEach((line, y) => {
          line.split('').forEach((s, x) => {
            if (s === 'x') {
              walls.push([x, y]);
            }
          });
        });
      }
      this.walls = walls;
      this.wallsPositions = walls.reduce((acc, [x, y]) => {
        acc[`${x}-${y}`] = true;
        return acc;
      }, {});
      this.speed = 300;
      this.interval = null;
      this.scores = 0;

      this.snake = new Snake(currentLevel.snake);

      this.canvas.width = this.screenSize[0] * this.zoom;
      this.canvas.height = this.screenSize[1] * this.zoom;
      this.initMushroom();
      this.draw();
      this.initInterval();
    } else {
      document.querySelector('#scores').innerHTML = 'You win! Molodec!';
    }
  }

  initInterval() {
    this.interval = setInterval(() => {
      this.step();
    }, this.speed);
  }

  initMushroom() {
    let x = this.snake.positions[0].x;
    let y = this.snake.positions[0].y;
    while (
      this.snake.positions.find(({x: sX, y: sY}) => x === sX && y === sY) || this.wallsPositions[`${x}-${y}`]
      ) {
      x = randomInteger(0, this.screenSize[0] - 1);
      y = randomInteger(0, this.screenSize[1] - 1);
    }
    this.mushroom = {
      x, y,
    };
  }

  keyPress(key) {
    this.snake.setDirection(key);
  }

  step() {
    if (!this.snake.move(this.screenSize, this.wallsPositions)) {
      this.gameOver();
    }
    const {
      x, y,
    } = this.mushroom;
    const { positions } = this.snake;

    if (x === positions[0].x && y === positions[0].y) {
      this.snake.eat();
      this.scores++;
      if (this.scores === this.target) {
        clearInterval(this.interval);
        this.level++;
        return this.init();
      }
      this.speed *= 0.95;
      this.initMushroom();
      clearInterval(this.interval);
      this.initInterval();
    }

    this.clear();
    this.draw();
  }

  static printScores(scores) {
    return ('000' + scores).split('').reverse().splice(0, 3).reverse().join('');
  }

  gameOver() {
    clearInterval(this.interval);
    alert(`Game over. Scores: ${Game.printScores(this.scores)}`);
  }

  clear() {
    this.ctx.clearRect(0, 0, this.screenSize[0] * this.zoom, this.screenSize[1] * this.zoom);
  }

  drawLines() {
    this.ctx.beginPath();
    for (let x = 0; x <= this.screenSize[0]; x++) {
      this.ctx.moveTo(x * this.zoom, 0);
      this.ctx.lineTo(x * this.zoom, this.screenSize[1] * this.zoom);
    }
    for (let y = 0; y <= this.screenSize[1]; y++) {
      this.ctx.moveTo(0, y * this.zoom);
      this.ctx.lineTo(this.screenSize[0] * this.zoom, y * this.zoom);
    }
    this.ctx.stroke();
  }

  draw() {
    this.drawLines();

    const {
      x, y,
    } = this.mushroom;
    const { positions } = this.snake;

    this.ctx.fillStyle = Game.musroomcolor;
    this.ctx.fillRect(x * this.zoom, y * this.zoom, this.zoom, this.zoom);

    positions.forEach((position, i) => {
      this.ctx.fillStyle = i === 0 ? Snake.headColor : Snake.color;
      this.ctx.fillRect(position.x * this.zoom, position.y * this.zoom, this.zoom, this.zoom);
    });

    this.walls.forEach(([x, y]) => {
      this.ctx.fillStyle = Game.wallcolor;
      this.ctx.fillRect(x * this.zoom, y * this.zoom, this.zoom, this.zoom);
    });

    document.querySelector('#scores').innerHTML =
      `Level: ${this.level}, scores: ${Game.printScores(this.scores)} / ${Game.printScores(this.target)}`;
  }
}
