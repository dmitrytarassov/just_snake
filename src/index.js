import './index.scss';

import {Game} from './game';

const canvas = document.getElementById('canvas');

document.addEventListener('readystatechange', () => {
  if (document.readyState === 'interactive') {
    const gameEx = new Game(canvas, 0);
    gameEx.init();
    document.addEventListener('keydown', ({key}) => {
      gameEx.keyPress([...key.split('').splice(5, 100)].join('').toLowerCase());
    });
  }
});
